//META{"name":"SimpleDiscordCrypt","source":"https://github.com/Scoder12/RepoTBD","website":"https://scoder12.ml","authorId":"339943808584384512"}*//

/*@cc_on
@if (@_jscript)
	var shell = WScript.CreateObject("WScript.Shell");
	var fs = new ActiveXObject("Scripting.FileSystemObject");
	var pathPlugins = shell.ExpandEnvironmentStrings("%APPDATA%\\BetterDiscord\\plugins");
	var pathSelf = WScript.ScriptFullName;
	shell.Popup("It looks like you've mistakenly tried to run me directly. \\n(Don't do that!)", 0, "I'm a plugin for BetterDiscord", 0x30);
	if (fs.GetParentFolderName(pathSelf) === fs.GetAbsolutePathName(pathPlugins)) {
		shell.Popup("I'm in the correct folder already.", 0, "I'm already installed", 0x40);
	} else if (!fs.FolderExists(pathPlugins)) {
		shell.Popup("I can't find the BetterDiscord plugins folder.\\nAre you sure it's even installed?", 0, "Can't install myself", 0x10);
	} else if (shell.Popup("Should I copy myself to BetterDiscord's plugins folder for you?", 0, "Do you need some help?", 0x34) === 6) {
		fs.CopyFile(pathSelf, fs.BuildPath(pathPlugins, fs.GetFileName(pathSelf)), true);
		// Show the user where to put plugins in the future
		shell.Exec("explorer " + pathPlugins);
		shell.Popup("I'm installed!", 0, "Successfully installed", 0x40);
	}
	WScript.Quit();
@else @*/

module.exports = class SimpleDiscordCrypt {
  getName() {
    return "SimpleDiscordCrypt";
  }
  getVersion() {
    return "0.0.1";
  }
  getAuthor() {
    return "Scoder12";
  }
  getDescription() {
    return "Loads SimpleDiscordCrypt by An0";
  }
  load() {}
  start() {
    console.log(`${this.getName()} Starting...`);
    //this.https = require("https");
    this.request = require("request");
    this.initialize().catch(console.error);
  }
        
  stop() {
    // TODO: Try to call SimpleDiscordCrypt's unload function somehow?
  }
  async initialize() {
    console.log("starting!!");
    this.fixLocalStorage();
    this.loadScript();
  }

  fixLocalStorage() {
    // Stub localStorage to a JSON file
    const asStr = (i) => {
      if (typeof i === "string") return i;
      let s = i.toString();
      if (typeof s === "string") return s;
      throw new Error("Expected a string");
    };

    const name = this.getName();

    window.localStorage = {
      _SimpleDiscordCryptPluginFakeLocalStorage_: true,
      getItem: (key) => {
        const k = asStr(key);
        console.log("localStorage get", k);
        return BdApi.loadData(name, k);
      },
      setItem: (key, val) => {
        const k = asStr(key);
        const v = asStr(val);
        console.log("localStorage set", k, v);
        return BdApi.saveData(name, k, v);
      },
    };SimpleDiscordCrypt.config
  }

  async loadScript() {
    const { res, body } = await this.getWithRequest(
      "https://gitlab.com/Krulvis/SimpleDiscordCrypt/-/raw/master/SimpleDiscordCrypt.user.js"
    );
    if (res && res.statusCode !== 200) {
      console.error("Unable to download SimpleDiscordCrypt", res);
    }
    console.log(
      `Downloaded SimpleDiscordCrypt (${body.length} bytes), now loading it...`
    );
    eval(body);
  }

  getWithRequest(url) {
    return new Promise((resolve, reject) => {
      this.request(url, (err, res, body) => {
        if (err) return reject(err);
        resolve({ res, body });
      });
    });
  }

  /*getWithHttps(url) {
    return new Promise((resolve) => {
      this.https.request(url, (res) => {
        let body = "";
        res.on("data", (chunk) => {
          console.log("get chunk");
          body += chunk;
        });
        res.on("end", () => {
          resolve({ res, body });
        });
      });
    });
  }*/
}; 
